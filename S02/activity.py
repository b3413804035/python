# Activity S02

# activity 1

year = int(input("Please enter a Year: "))

if year % 100 == 0 and year % 400 == 0:
    print(year, "Is a Leap Year")
elif year % 4 == 0 and year % 100 != 0:
    print(year, "Is a Leap Year")
elif year == 0:
    print("No Zeros")
elif year < 0:
    print("No Negative Value")
else:
    print(year, "Is not a Leap Year")


# Activit 2

row = int(input("Enter the number of rows: "))
col = int(input("Enter the number of columns: "))

grid = ""
for i in range(row):
    for j in range(col):
        grid += "*"
    grid += '\n'

print(grid)




