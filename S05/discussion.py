from abc import ABC, abstractmethod

class Person(ABC):

    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def addRequest(self):
        pass

    @abstractmethod
    def checkRequest(self):
        pass

    @abstractmethod
    def addUser(self):
        pass
    
class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def set_firstName(self, firstName):
        self._firstName = firstName

    def get_firstName(self):
        print(f"{self._firstName}")

    def set_lastName(self, lastName):
        self._lastName = lastName

    def get_lastName(self):
        print(f"{self._lastName}")

    def set_email(self, email):
        self._email = email

    def get_email(self):
        print(f"{self._email}")

    def set_department(self, department):
        self._department = department

    def get_department(self):
        print(f"{self._department}")

    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")

    def addRequest(self):
        print("Request has been added")

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []
    
    def set_firstName(self, firstName):
        self._firstName = firstName

    def get_firstName(self):
        print(f"{self._firstName}")

    def set_lastName(self, lastName):
        self._lastName = lastName

    def get_lastName(self):
        print(f"{self._lastName}")

    def set_email(self, email):
        self._email = email

    def get_email(self):
        print(f"{self._email}")

    def set_department(self, department):
        self._department = department

    def get_department(self):
        print(f"{self._department}")

    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")

    def addRequest(self):
        print("Request has been added")

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

    def addMember(self, member):
        self._members.append(member)
    
    def get_members(self):
        return self._members

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def set_firstName(self, firstName):
        self._firstName = firstName

    def get_firstName(self):
        print(f"{self._firstName}")

    def set_lastName(self, lastName):
        self._lastName = lastName

    def get_lastName(self):
        print(f"{self._lastName}")

    def set_email(self, email):
        self._email = email

    def get_email(self):
        print(f"{self._email}")

    def set_department(self, department):
        self._department = department

    def get_department(self):
        print(f"{self._department}")

    def getFullName(self):
        print(f"{self._firstName} {self._lastName}")

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        print("New user added")

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

class Request():
    def __init__(self, name, requester, dataRequested):
        self._name = name
        self._requester = requester
        self._dateResquested = dataRequested
        self._status = []

    def updateRequest(self):
        print(f"{self._name} Request has been updated")
    
    def closeRequest(self):
        print(f"{self._name} Request has been closed")

    def cancelReqeust(self):
        print(f"{self._name} Request has been cancelled")

    def set_status(self, status):
        self._status = {status}
        


#Test Case
employee1 = Employee ("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee ("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee ("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee ("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin ("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Micheal", "Specter", "smicheal@mail.com", "Sales")
req1 = Request ("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request ("Laptop repair", employee1, "1-Jul-2021")

# assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
# assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
# assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
# assert employee2.login() == "sjane@mail.com has logged in"
# assert employee2.addRequest() == "Request has been added"
# assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print (indiv_emp.getFullName())

admin1.addUser()
req2.set_status('closed')
print(req2.closeRequest())

