# [Section] Comments
# Comments in the Python are done using "#" symbol

# [Section] Python Syntax

print("Hello World!")

# [Section] Indentation
# indentation in Python is very important
# In Python, indentation is used to indicate a block of code

# [Section] Variables
# The terminology used for variable names is "identifier"
# The Python, a variable is declared by stating the variable name and assingning a value using the equality symbol

# [Section] Naming Convention
# Python uses the snake case convention


age = 35
print(age)
middle_initial = "C"
print(middle_initial)

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name4)

# [Section] Data Types
# 1. String(str) - for alphanumeric and Symbols
full_name = "John Doe"
print(full_name)

secret_code = 'Pa$$word'
print(secret_code)

#  2. Number(int, float, complex) - for integers, decimals, and complex numbers
num_of_days = 365 # this is an integer
print(num_of_days)
pi_approx = 3.1416 # this is a decimal
print(pi_approx)
complex_num = 1 + 5j # this is a complex number
print(complex_num)
print(complex_num.real)
print(complex_num.imag)

# Boolean - for truth values

is_learning = True
isDiffuclt = False
print(is_learning)
print(isDiffuclt)

# [Section] Using Variables
# Just like in JS, Variable are used by simply calling the name of the identifies
# To use the variables, concatenation ("+") symbol between strings can be used
print("My name is " + full_name)
# this returns a "TypeError" as numbers can't be concatenated to strings
# print("My age is " + age)
print("My age is " + str(age))

# [Section] Typecasting
# There may be times when we want to specify a type on to a variable. this can be done with casting/ here are some functions that can be used:
# 1. int()
# 2. float()
# 3. str()
print(int(3.5))
print(float(3))

# Another way to avoid the type error in printing without the use of typecasting is the use of F-string
# to use F-string, add a lowercase "f" before the string and place the desired variable in {}
print(f"Hi, my name is {full_name} and my age is {age}")

# [Section] Operations 
# python has operator families that can be used to manipulate variable
# Arithmetic Operators = perform mathematical operations
print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4)
print(2 ** 6)

# Assignment Operators - 
num1 = 3
print(num1)
num1 += 4
print(num1)
# Other assignment operators (-=, *=, /=, %=)

# Comparison Operators
print(1 == 1) # True
print(1 == "1") # False
# other operations (!=, >=, <=, >, <)

# Logical operators - used to combine conditional statements
print (True and False)
print(not False)
print(False or True)