# activity S01

name = "Jose"
age = 38
occupation = "Writer"
movie = "The Avengers: Endgame"
rating = 99.8

print(f"I am {name}, and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating} %")

num1, num2, num3 = 20, 15, 10 
print(num1 * num2)
print(num1 < num3)
print(num3 + num2)
