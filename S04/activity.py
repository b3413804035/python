from abc import ABC, abstractmethod

class Animal(ABC):

    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass


class Dog(Animal):
    def __init__(self):
        super().__init__()

    def eat(self):
        print("Eaten Steak")

    def make_sound(self):
        print("Bark! woof! arf!")

    def call(self):
        print(f"Here {self._name}")

    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(self._name)

    def set_breed(self, breed):
        self.set_breed = breed

    def get_breed(self):
        print(self._breed)
    
    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(self._age)

class Cat(Animal):
    def __init__(self):
        super().__init__()

    def eat(self):
        print("Serve me Tuna")

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaa!")
    
    def call(self):
        print(f"{self._name}, come on!")

    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(self._name)

    def set_breed(self, breed):
        self.set_breed = breed

    def get_breed(self):
        print(self._breed)

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(self._age)

dog = Dog()
cat = Cat()

# Dog
dog._name = "Iris"
dog._breed = "Akita"
dog._age = 5
dog.eat()
dog.make_sound()
dog.call()

# Cat
cat._name = "Puss"
cat._breed = "American Wirehair"
cat._age = 4
cat.eat()
cat.make_sound()
cat.call()